import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";

import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue"; 
import NotFound from "@/pages/NotFoundPage.vue";
import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import Notifications from "@/pages/Notifications.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Typography from "@/pages/Typography.vue";
import TableList from "@/pages/TableList.vue";

Vue.use(VueRouter);

// configure router
// const router = new VueRouter({
//   routes, // short for routes: routes
//   linkActiveClass: "active"
// });

// export default router;

export default new VueRouter({
  mode: "history",  
  linkActiveClass: "active",
  routes: [
   
    {
        path: "/login",
        name: "Login",
        component: () => import("@/views/Login.vue"), 
        meta: {
            requireAuth: false
        }
    },  

    {
      path: "/",
      component: DashboardLayout,
      redirect: "/customer",
      children: [ 
        {
          path: "customer",
          name: "customer",
          component: () => import("@/views/customer/List.vue"),
          meta: {
              requireAuth: true
          }, 
        },
        {
          path: "customer/add",
          name: "customer add",
          component: () => import("@/views/customer/Add.vue"),
          meta: {
              requireAuth: true
          }
        },
        {
          path: "customer/vehicle",
          name: "vehicles",
          component: () => import("@/views/vehicle/List.vue"),
          meta: {
              requireAuth: true
          }
        },
        {
          path: "customer/vehicle/add",
          name: "customer/vehicle add",
          component: () => import("@/views/vehicle/Add.vue"),
          meta: {
              requireAuth: true
          }
        },
        {
          path: "customer/vehicle/insurance",
          name: "insurance",
          component: () => import("@/views/insurance/List.vue"),
          meta: {
              requireAuth: true
          }
        },
        {
          path: "customer/vehicle/insurance/add",
          name: "add insurance",
          component: () => import("@/views/insurance/Add.vue"),
          meta: {
              requireAuth: true
          }
        }, 
        {
          path: "customer/vehicle/insurance/finalize",
          name: "finalize insurance",
          component: () => import("@/views/insurance/Finalize.vue"),
          meta: {
              requireAuth: true
          }
        },
        {
          path: "customer/vehicle/insurance/detail",
          name: "insurance detail",
          component: () => import("@/views/insurance/Detail.vue"),
          meta: {
              requireAuth: true
          }
        },  
        
        {
          path: "dashboard",
          name: "dashboard",
          component: () => import("@/views/dashboard/Dashboard.vue"),
          meta: {
              requireAuth: true
          }
        },

        // {
        //   path: "dashboard",
        //   name: "dashboard",
        //   component: Dashboard,
        //   meta: {
        //       requireAuth: true
        //   }
        // },
        {
          path: "stats",
          name: "stats",
          component: UserProfile,
          meta: {
              requireAuth: true
          }
        },
        {
          path: "notifications",
          name: "notifications",
          component: Notifications,
          meta: {
              requireAuth: true
          }
        },
        {
          path: "icons",
          name: "icons",
          component: Icons,
          meta: {
              requireAuth: true
          }
        },
        {
          path: "maps",
          name: "maps",
          component: Maps,
          meta: {
              requireAuth: true
          }
        },
        {
          path: "typography",
          name: "typography",
          component: Typography,
          meta: {
              requireAuth: true
          }
        },
        {
          path: "table-list",
          name: "table-list",
          component: TableList,
          meta: {
              requireAuth: true
          }
        }
      ]
    },
    { path: "*", component: NotFound } 
    
  ],

});