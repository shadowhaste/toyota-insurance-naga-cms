import insuranceService from "../services/insurance";  
import notificationService from "../services/notification";  
import genericService from "../services/generic";

export default {
    namespaced: true,
    state: { 
        loading: false,
        errors: null, 

        list: [],
        data: null,
        page: 1,
        currentPage: 1,
        prevPage: 0,
        nextPage: 0,
        maxPage: 1, 
        totalPerPage: 1,
        currentFirstItem: 1,
        currentLastItem: 1,
        initialPageCount: 0,
        insuranceObj: null,   
    },

    mutations: {
        loading(state, value) {
            state.loading = value;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        }, 

        setList(state, response) {
            state.list = response;
            state.loading = false;
        },

        setData(state, data){
            state.data = data;  
        }, 

        setInsurance(state, insuranceObj){
            state.insuranceObj = insuranceObj;  
        }, 

        setInitialPageCount(state, list) {
            state.initialPageCount = list.length;
        },
        pagination(state, pagination) {
            state.totalPerPage = pagination.list.length;
            state.maxPage = pagination.max_page;
            state.prevPage = pagination.prev_page;
            state.nextPage = pagination.next_page;
            state.currentPage = state.prevPage + 1;
            state.currentFirstItem = state.initialPageCount * state.prevPage + 1;
            state.currentLastItem = state.currentFirstItem > 0
                    ? state.currentFirstItem + state.totalPerPage - 1
                    : 0;
        },

    },
 
    actions:{  
 
        getList: async ({ commit, state }, page) => {
            commit("loading", true); 
            try {   
                let response = await insuranceService.getList(state.data, page);  
                commit("setList", response.list);
                commit("pagination", response);
                commit("loading", false);
            } catch (errors) {
                commit("errors", errors);
                commit("loading", false);
            }
        },

        create: async ({ commit, state }, param) => {          
            try {
                let response = await insuranceService.create(state.data);                 
                notificationService.successModal(response.message);  
                commit("errors", null);
            } catch (errors) { 
                if(errors.status == 422){
                    notificationService.errorModal("Incomplete information, Review your answers then try to save again. Thanks");  
                } 

                if(errors.status == 400){
                    notificationService.errorModal(errors.errors.message);
                }  

                commit("errors", errors);
            }
        },

        finalize: async ({ commit, state }, param) => {          
            try {
                let response = await insuranceService.finalize(state.data);                 
                notificationService.successModal(response.message);  
                commit("errors", null);
            } catch (errors) { 
                if(errors.status == 422){
                    notificationService.errorModal("Incomplete information, Review your answers then try to save again. Thanks");  
                } 

                if(errors.status == 400){
                    notificationService.errorModal(errors.errors.message);
                }  

                commit("errors", errors);
            }
        },

        view: async ({ commit, state }) => {          
            try {  
                let response = await genericService.postRequest("insurance/view", state.data);   
                commit("setInsurance", response.insurance);
            } catch (errors) {  
                commit("errors", errors);
            }
        },

    }
    
}