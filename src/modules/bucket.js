import bucketService from "../services/bucket.js"; 
import notification from '../services/notification.js';
import router from "@/router/index";

export default {
    namespaced: true,
    state: {
        loading: false,
        errors: null, 
        buckets: [],
        detail: {
            name: null, 
            description: null,
            rows: null,
            columns: null
        }
    },

    mutations: {
        loading(state) {
            state.loading = true;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        }, 

        buckets(state, buckets) {
            state.buckets = buckets;
            state.loading = false; 
        },

        setDetails(state, value) {   
            state.detail.name = value.name; 
            state.detail.description = value.description; 
            state.detail.rows = value.rows; 
            state.detail.columns = value.columns; 
        },  
    }, 
 
    actions: {

        getBucketList: async ({ commit, state }, page) => { 
            commit("loading");

            try { 
                let response = await bucketService.getBucketList(); 
                
                let result = response.data.list.data;
                let list = [];
                result.map(function(bucket, key) { 
                    console.log(bucket);
                    list.push(
                        {
                            uuid: bucket.uuid, 
                            description: bucket.description,
                            rows: bucket.rows,
                            columns: bucket.columns,                            
                            title: bucket.name,
                            value: bucket.rows,                            
                            footerText: "View",
                            footerIcon: "ti-view-list",
                            type: "success",
                            icon: "ti-server",
                        },
                    )
                }); 
                await commit("buckets", list); 
            } catch (errors) {
                commit("errors", errors);
            }
        },

        createBucket: async ({ commit, state }) => {
            commit("loading"); 
            try {  
                let response = await bucketService.createBucket(
                    state.detail,
                    state.errors = null
                );  
                notification.successModal(response.message); 
                router.push("/bucket");
            } catch (errors) { 
                 
                if(errors.status == 400){
                    notification.errorModal(errors.errors.message);
                }
                commit("errors", errors); 
            }
        }
    }, 
}