import notificationService from "../services/notification";  
import genericService from "../services/generic";

export default {
    namespaced: true,
    state: { 
        loading: false,
        errors: null,  
        data: null,  
        proceed_payment: 0,
        payment: null
    },

    mutations: {
        loading(state, value) {
            state.loading = value;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        }, 
 

        setData(state, data){
            state.data = data;  
        }, 

        setProceedPayment(state, data){
            state.proceed_payment = data;  
        },  

        setPayment(state, data){
            state.payment = data;  
        }, 


    },
 
    actions:{  
  
        check: async ({ commit, state }) => {          
            try {  
                let response = await genericService.postRequest("payment/check/amortisation", state.data);    
                commit("setProceedPayment", response.proceed);
            } catch (errors) {  

                if(errors.status == 422){
                    notificationService.errorModal("Incomplete information, Review your answers then try to save again. Thanks");  
                } 

                if(errors.status == 400){
                    notificationService.errorModal(errors.errors.message);
                }   
                commit("setProceedPayment", 0); 
                commit("errors", errors);
            }
        },

        pay: async ({ commit, state }) => {          
            try {  
                let response = await genericService.postRequest("payment/add", state.data);    
                notificationService.successModal(response.message);  
                commit("errors", null);
                commit("setPayment", response.model);
            } catch (errors) {  

                // if(errors.status == 422){
                //     notificationService.errorModal("Incomplete information, Review your answers then try to save again. Thanks");  
                // } 

                if(errors.status == 400){
                    notificationService.errorModal(errors.errors.message);
                }    
                    
                commit("errors", errors);
            }
        },

    }
    
}