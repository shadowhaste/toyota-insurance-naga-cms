import notificationService from "../services/notification";  
import genericService from "../services/generic";

export default {
    namespaced: true,
    state: { 
        loading: false,
        errors: null,  
        dataCounts: null,    

        preferencesChart: {
            data: null,
            options: {}
        }
    },

    mutations: {
        loading(state, value) {
            state.loading = value;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        }, 
 

        setData(state, data){
            state.dataCounts = data;  

            let quoteLabel = data.avg_quotes + "%";
            let finalLabel = data.avg_final + "%";

            state.preferencesChart.data = {
                labels: [quoteLabel, finalLabel],
                series: [data.avg_quotes, data.avg_final] 
            }
        },   
 
    },
 
    actions:{  
  
        view: async ({ commit, state }) => {          
            try {  
                let response = await genericService.postRequest("analytics/counts", state.data);    
                commit("setData", response);
            } catch (errors) {  
 
                commit("errors", errors);
            }
        },

    }
    
}