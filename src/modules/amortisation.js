import notificationService from "../services/notification";  
import genericService from "../services/generic";

export default {
    namespaced: true,
    state: { 
        loading: false,
        errors: null,  
        data: null,  
        proceed_payment: 0,
        amortisation: null
    },

    mutations: {
        loading(state, value) {
            state.loading = value;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        }, 
 

        setData(state, data){
            state.data = data;  
        }, 
 

        setAmortisation(state, data){
            state.amortisation = data;  
        }, 


    },
 
    actions:{  
  
        view: async ({ commit, state }) => {          
            try {  
                let response = await genericService.postRequest("insurance/amortisation/view", state.data);    
                commit("setAmortisation", response.model);
                commit("errors", null);
            } catch (errors) {   
                commit("errors", errors);
            }
        },
 

    }
    
}