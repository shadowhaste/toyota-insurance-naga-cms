import customerService from "../services/customer";  
import notificationService from "../services/notification";  

export default {
    namespaced: true,
    state: { 
        loading: false,
        errors: null, 

        list: [],
        data: null,
        page: 1,
        currentPage: 1,
        prevPage: 0,
        nextPage: 0,
        maxPage: 1, 
        totalPerPage: 1,
        currentFirstItem: 1,
        currentLastItem: 1,
        initialPageCount: 0,  
        customer: null,
    },

    mutations: {
        loading(state, value) {
            state.loading = value;
        },

        errors(state, errors) {
            state.errors = errors;
            state.loading = false;
        }, 

        setList(state, response) {
            state.list = response;
            state.loading = false;
        },

        setData(state, data){
            state.data = data; 
        }, 

        setCustomer(state, data){ 
            state.customer = data;   
        }, 

        setInitialPageCount(state, list) {
            state.initialPageCount = list.length;
        },
        pagination(state, pagination) {
            state.totalPerPage = pagination.list.length;
            state.maxPage = pagination.max_page;
            state.prevPage = pagination.prev_page;
            state.nextPage = pagination.next_page;
            state.currentPage = state.prevPage + 1;
            state.currentFirstItem = state.initialPageCount * state.prevPage + 1;
            state.currentLastItem = state.currentFirstItem > 0
                    ? state.currentFirstItem + state.totalPerPage - 1
                    : 0;
        },

    },
 
    actions:{  
 
        getList: async ({ commit, state }, page) => {
            commit("loading", true); 
            try {  
                let response = await customerService.getList(state.data, page);  
                commit("setList", response.list);
                commit("pagination", response);
                commit("loading", false);
            } catch (errors) {
                commit("errors", errors);
                commit("loading", false);
            }
        },

        create: async ({ commit, state }, param) => {          
            try {
                let response = await customerService.create(state.data); 
                notificationService.successModal(response.message);  
                
                //update list 
                // state.list.push(response.model);
            } catch (errors) { 
                if(errors.status == 422){
                    notificationService.errorModal("Incomplete information, Please press previous button to review your answers then try to save again. Thanks");  
                }
                commit("errors", errors);
            }
        },

        view: async ({ commit, state }) => {          
            try {
                let response = await customerService.view(state.data);   
                commit('setCustomer', response.customer);
            } catch (errors) {  
                commit("errors", errors);
            }
        },
        

    }
    
}