import api from "./api";
import auth from "./auth";

export default {   
    getList(data, page = 1) { 
        return new Promise((resolve, reject) => {
            api.post("vehicle/list/" + page, 
                data,
                auth.getHeader()
            )
            .then(response => {
                return resolve(response);
            })
            .catch((status, error) => {
                console.log(error);
                reject(status, error);
            });
        });
    },

    processCreate(data){  
        return new Promise((resolve, reject) => {
            api.post(
                "vehicle/add", 
                data,
                auth.getHeader()

            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    },

    view(data){  
        return new Promise((resolve, reject) => {
            api.post(
                "vehicle/view", 
                data,
                auth.getHeader()

            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    },

}