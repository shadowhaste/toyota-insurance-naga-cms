import api from "./api";
import auth from "./auth";

export default { 
   
    getBucketList() { 
        return new Promise((resolve, reject) => {
            api.post(
                "bucket/list",
                [],
                auth.getHeader()
            ).then(response => {
                return resolve(response);
            })
            .catch((error) => {   
                return reject(error);
            });
        });
    },   
    
    createBucket(detail) { 
        return new Promise((resolve, reject) => {
            api.post(
                "bucket/create",
                detail, 
                auth.getHeader()
            ).then(response => {
                return resolve(response);
            })
            .catch((error) => {   
                return reject(error);
            });
        });
    },  

}