import api from "./api";
import auth from "./auth";

export default {   
    getList(data, page = 1) { 
        return new Promise((resolve, reject) => {
            api.post("insurance/list/" + page, 
                data,
                auth.getHeader()
            )
            .then(response => {
                return resolve(response);
            })
            .catch((status, error) => {
                console.log(error);
                reject(status, error);
            });
        });
    },

    create(data){  
        return new Promise((resolve, reject) => {
            api.post(
                "insurance/add", 
                data,
                auth.getHeader()

            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    },

    finalize(data){  
        return new Promise((resolve, reject) => {
            api.post(
                "insurance/finalize", 
                data,
                auth.getHeader()

            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    }, 

}