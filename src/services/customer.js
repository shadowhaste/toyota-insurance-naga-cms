import api from "./api";
import auth from "./auth";

export default {   
    getList(data, page = 1) { 
        return new Promise((resolve, reject) => {
            api.post("customer/list/" + page, 
                data,
                auth.getHeader()
            )
            .then(response => {
                return resolve(response);
            })
            .catch((status, error) => {
                console.log(error);
                reject(status, error);
            });
        });
    },

    create(data){  
        return new Promise((resolve, reject) => {
            api.post(
                "customer/add", 
                data,
                auth.getHeader()

            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    },

    view(data){  
        return new Promise((resolve, reject) => {
            api.post(
                "customer/view", 
                data,
                auth.getHeader()

            ).then(response => {  
                return resolve(response);
            }).catch((status, error) => { 
                reject(status, error);
            });
        });
    },
 
}